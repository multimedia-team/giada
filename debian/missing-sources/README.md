missing sources for giada
=========================

this directory contains sources that are missing from upstream's tarball.


mcl-atomic-swapper
==================

is a tiny (1 source file) module released under the GPLv3+,
maintained by giada upstream in a separate repository.

mcl-audio-buffer
================

is a tiny (2 source files) module released under the GPLv3+,
maintained by giada upstream in a separate repository.

