Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Giada
Upstream-Contact: Giovanni A. Zuliani | Monocasual <giadaloopmachine@gmail.com>
Source: https://www.giadamusic.com

Files: *
Copyright:
 2010-2022 Giovanni A. Zuliani | Monocasual
License: GPL-3+

Files: extras/com.giadamusic.Giada.metainfo.xml
Copyright:
 2010-2021 Giovanni A. Zuliani | Monocasual
License: CC0
 On Debian systems, the complete text of the CC0 license
 can be found in /usr/share/common-licenses/CC0-1.0.

Files: debian/*
Copyright:
 2018-2020 Olivier Humbert <trebmuh@tuxfamily.org>
 2015-2017 Jaromír Mikeš <mira.mikes@seznam.cz>
 2015-2024 IOhannes m zmölnig <umlaeute@debian.org>
 2020-2022 Dennis Braun <d_braun@kabelmail.de>
License: GPL-3+

Files: debian/missing-sources/mcl-*
Copyright:
 2021 Giovanni A. Zuliani | Monocasual
License: GPL-3+

License: GPL-3+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
Comment: You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-3'.
